function init() {
    var user_email = '';
    var loginUser;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic_signin');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = '<a class="nav-link" href="#" id="logout-btn">Logout</a>';
            document.getElementById("dynamic_user").innerHTML = '<a class="nav-link" href="dashboard.html" id="user-btn">'+user.displayName+'</a>';
            loginUser = firebase.auth().currentUser.uid;
            console.log("change logout");
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            document.getElementById("logout-btn").addEventListener("click", function () {
                firebase.auth().signOut().then(function () {
                    //create_alert("success", "");
                    alert("sueccess");
                }).catch(function (error) {
                    //create_alert("error", error.code, error.message);
                    alert("failed");
                });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = '<a class="nav-link" href="signin.html">Login</a>';
            document.getElementById("dynamic_user").innerHTML = "";
            loginUser = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt1 = document.getElementById('inputName');
    post_txt2 = document.getElementById('inputEmail');
    post_txt3 = document.getElementById('inputPhone');
    post_txt4 = document.getElementById('inputAddr');
    post_txt5 = document.getElementById('inputNum');

    post_btn.addEventListener('click', function () {
        
        if (post_txt5.value > 0 && loginUser) {
            var newpostref = firebase.database().ref('list/' + loginUser).push();
            newpostref.set({
                name: post_txt1.value,
                email: post_txt2.value,
                phone: post_txt3.value,
                addr: post_txt4.value,
                num: post_txt5.value,
                size: "128G"
            }).then(function (){
                //alert("success");
                window.location = 'check.html';
            });
            post_txt1.value = "";
            post_txt2.value = "";
            post_txt3.value = "";
            post_txt4.value = "";
            post_txt5.value = "";
        }
        else if (post_txt5.value < 1){
            alert("數量需大於0");
        }
        else{
            
            window.location = "signin.html";
            alert("請先登入");
        }
    });
}

window.onload = function () {
    init();
};
