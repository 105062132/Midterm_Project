function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic_signin');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = '<a class="nav-link" href="#" id="logout-btn">Logout</a>';
            document.getElementById("dynamic_user").innerHTML = '<a class="nav-link" href="dashboard.html" id="user-btn">'+user.displayName+'</a>';
            console.log("change logout");
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            document.getElementById("logout-btn").addEventListener("click", function () {
                firebase.auth().signOut().then(function () {
                    //create_alert("success", "");
                    alert("sueccess");
                }).catch(function (error) {
                    //create_alert("error", error.code, error.message);
                    alert("failed");
                });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = '<a class="nav-link" href="signin.html">Login</a>';
            document.getElementById("dynamic_user").innerHTML = "";
        }
    });

    
}

window.onload = function () {
    init();
    var notifyConfig = {
        body: 'You have been permitted notification!', 
      };
      if (Notification.permission === 'default' || Notification.permission === 'undefined' || Notification.permission === 'granted') {
        Notification.requestPermission(function (permission) {
        var notification = new Notification('Hey!', notifyConfig);
        })
      }
};