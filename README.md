# Software Studio 2018 Spring Midterm Project
[網址:https://105062132.gitlab.io/Midterm_Project/index.html](https://105062132.gitlab.io/Midterm_Project/index.html)
[報告:https://gitlab.com/105062132/Midterm_Project/blob/master/README.md](https://gitlab.com/105062132/Midterm_Project/blob/master/README.md)
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Memory Card Shopping]
* Key functions (add/delete)
    1. 產品介紹
    2. 可線上訂購
    3. 個人資料頁面
    
* Other functions (add/delete)
    1. Notification
    2. 可看到購物紀錄
    3. RWD
    4. 賣家評價
    5. 重設密碼
    

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|N|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
1. bootstrap template:
[https://startbootstrap.com/template-overviews/shop-item/](https://startbootstrap.com/template-overviews/shop-item/)
2. 線上訂購使用database的set function
3. 取消訂單使用database的set(null)function
4. Notification :在進入首頁會跳出"Hey"的通知
5. 個人資料頁面:會顯示個人的基本資料
   使用database.once()function
6. 購物紀錄:會顯示使用者歷史的購物紀錄
7. 賣家評價:登入時可以留言，所有人都可以看到留言
8. 重設密碼:忘記密碼時，可透過重設密碼的信來重設
## Security Report (Optional)
Database裡有三個節點，分別是user、list、和com_list。user存放個人資料；list存放購物歷史，因此下面的節點的名稱都是每個帳戶的uid，權限都要是$uid === auth.uid才可以讀寫，因此外部的人無法讀取。
另外，也把預設的localhost拿掉，避免其他人把網站的檔案下載後拿去更改，建立新的帳戶來更改或讀取裡面的資料。