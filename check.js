function init() {
    var user_email = '';
    var loginUser;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic_signin');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = '<a class="nav-link" href="#" id="logout-btn">Logout</a>';
            loginUser = firebase.auth().currentUser.uid;
            console.log("change logout");
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            document.getElementById("logout-btn").addEventListener("click", function () {
                firebase.auth().signOut().then(function () {
                    //create_alert("success", "");
                    alert("sueccess");
                }).catch(function (error) {
                    //create_alert("error", error.code, error.message);
                    alert("failed");
                });
            });
            //----
            var database = firebase.database().ref('list/' + loginUser);
            var dataname;
            var dataemail;
            var dataphone; 
            var dataaddr;
            var datanum;
            var datasize;
            var first_count = 0;
            var second_count = 0;
            var datakey;
            
            database.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                
            
                dataaddr = childSnapshot.val().addr;
                dataemail = childSnapshot.val().email;
                dataname = childSnapshot.val().name;
                console.log(dataname);
                datanum = childSnapshot.val().num;
                dataphone = childSnapshot.val().phone;
                datasize = childSnapshot.val().size;
                first_count +=1;
                datakey = childSnapshot.key;
                
            });
            

            document.getElementById("datalist").innerHTML = '<p class="card-text">姓名:' + dataname + '</p>' + 
                '<p class="card-text">Email:' + dataemail + '</p>' + 
                '<p class="card-text">電話號碼:' + dataphone + '</p>' +
                '<p class="card-text">地址:' + dataaddr + '</p>' + 
                '<p class="card-text">記憶卡容量:' + datasize + '</p>' + 
                '<p class="card-text">數量:' + datanum + '</p>' + 
                '<button class="btn btn-lg btn-primary btn-block" id="post_btn">確認送出</button>' + 
                '<button class="btn btn-lg btn-secondary btn-block" id="cancel_btn">取消</button>';
            document.getElementById("post_btn").addEventListener("click", function (){
                alert("訂購成功");
                window.location = "index.html";
            });
            document.getElementById("cancel_btn").addEventListener("click", function () {
                firebase.database().ref('list/' + loginUser  + '/'+ datakey).set(null);
                alert("取消成功");
                window.location = "index.html";
            });
            //console.log("once");
            //console.log(dataname);
            
            /*database.on('child_added', function (data) {
                second_count += 1;
                console.log("on");
                if (second_count > first_count){
                    dataaddr = data.val().addr;
                    dataemail = data.val().email;
                    dataname = data.val().name;
                    console.log(dataname);
                    datanum = data.val().num;
                    dataphone = data.val().phone;
                    datasize = data.val().size;
                    document.getElementById("datalist").innerHTML = '<p class="card-text">姓名:' + dataname + '</p>' + 
                    '<p class="card-text">Email:' + dataemail + '</p>' + 
                    '<p class="card-text">電話號碼:' + dataphone + '</p>' +
                    '<p class="card-text">地址:' + dataaddr + '</p>' + 
                    '<p class="card-text">記憶卡容量:' + datasize + '</p>' + 
                    '<p class="card-text">數量:' + datanum + '</p>';
                }
            });*/
        }).catch(e => console.log(e.message));

        } else {
            // It won't show any post if not login
            menu.innerHTML = '<a class="nav-link" href="signin.html">Login</a>';
        }
    });

    var database = firebase.database().ref('list/' + loginUser);
    var dataname;
    var dataemail;
    var dataphone; 
    var dataaddr;
    var datanum;
    var datasize;
    var first_count = 0;
    var second_count = 0;
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    //console.log(loginUser);
    var str_after_content = "</p></div></div>\n";
    var total_post = [];
    
    /*database.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                data_addr = childData.addr;
                data_email = childData.email;
                data_name = childData.name;
                console.log(data_name);
                data_num = childData.num;
                data_phone = childData.phone;
                data_size = childData.size;
                total_post[total_post.length] = '<p class="card-text">姓名:' + data_name + '</p>' + 
                '<p class="card-text">Email:' + data_email + '</p>' + 
                '<p class="card-text">電話號碼:' + data_phone + '</p>' +
                '<p class="card-text">地址:' + data_addr + '</p>' + 
                '<p class="card-text">記憶卡容量:' + data_size + '</p>' + 
                '<p class="card-text">數量:' + data_num + '</p>';
                first_count += 1;
            });
            document.getElementById('datalist').innerHTML = total_post.join('');

            //add listener
            database.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    data_addr = childData.addr;
                data_email = childData.email;
                data_name = childData.name;
                console.log(data_name);
                data_num = childData.num;
                data_phone = childData.phone;
                data_size = childData.size;
                    total_post[total_post.length] = '<p class="card-text">姓名:' + data_name + '</p>' + 
                    '<p class="card-text">Email:' + data_email + '</p>' + 
                    '<p class="card-text">電話號碼:' + data_phone + '</p>' +
                    '<p class="card-text">地址:' + data_addr + '</p>' + 
                    '<p class="card-text">記憶卡容量:' + data_size + '</p>' + 
                    '<p class="card-text">數量:' + data_num + '</p>';
                    document.getElementById('datalist').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));*/
    /*database.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                
            
                dataaddr = childSnapshot.val().addr;
                dataemail = childSnapshot.val().email;
                dataname = childSnapshot.val().name;
                console.log(data_name);
                datanum = childSnapshot.val().num;
                dataphone = childSnapshot.val().phone;
                datasize = childSnapshot.val().size;
                first_count +=1;
                
            });
            document.getElementById("datalist").innerHTML = '<p class="card-text">姓名:' + dataname + '</p>' + 
                '<p class="card-text">Email:' + dataemail + '</p>' + 
                '<p class="card-text">電話號碼:' + dataphone + '</p>' +
                '<p class="card-text">地址:' + dataaddr + '</p>' + 
                '<p class="card-text">記憶卡容量:' + datasize + '</p>' + 
                '<p class="card-text">數量:' + datanum + '</p>';
            console.log("once");
            console.log(dataname);
            
            database.on('child_added', function (data) {
                second_count += 1;
                console.log("on");
                if (second_count > first_count){
                    data_addr = data.val().addr;
                    data_email = data.val().email;
                    data_name = data.val().name;
                    console.log(data_name);
                    data_num = data.val().num;
                    data_phone = data.val().phone;
                    data_size = data.val().size;
                    document.getElementById("datalist").innerHTML = '<p class="card-text">姓名:' + data_name + '</p>' + 
                    '<p class="card-text">Email:' + data_email + '</p>' + 
                    '<p class="card-text">電話號碼:' + data_phone + '</p>' +
                    '<p class="card-text">地址:' + data_addr + '</p>' + 
                    '<p class="card-text">記憶卡容量:' + data_size + '</p>' + 
                    '<p class="card-text">數量:' + data_num + '</p>';
                }
            });
        }).catch(e => console.log(e.message));*/
    /*database.once('value', function(snapshot){
        snapshot.forEach(function(childSnapshot){
            data_addr = data_email = data_name = data_num = data_phone = data_size= '';
            
            data_addr = childSnapshot.val().addr;
            data_email = childSnapshot.val().email;
            data_name = childSnapshot.val().name;
            console.log(data_name);
            data_num = childSnapshot.val().num;
            data_phone = childSnapshot.val().phone;
            data_size = childSnapshot.val().size;

            document.getElementById("datalist").innerHTML = '<p class="card-text">姓名:' + data_name + '</p>' + 
            '<p class="card-text">Email:' + data_email + '</p>' + 
            '<p class="card-text">電話號碼:' + data_phone + '</p>' +
            '<p class="card-text">地址:' + data_addr + '</p>' + 
            '<p class="card-text">記憶卡容量:' + data_size + '</p>' + 
            '<p class="card-text">數量:' + data_num + '</p>';
            
        })
    });*/
    
}

window.onload = function () {
    init();
};
