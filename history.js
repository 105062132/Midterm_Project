function init() {
    var user_email = '';
    var loginUser;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic_signin');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = '<a class="nav-link" href="#" id="logout-btn">Logout</a>';
            document.getElementById("dynamic_user").innerHTML = '<a class="nav-link" href="dashboard.html" id="user-btn">'+user.displayName+'</a>';
            loginUser = firebase.auth().currentUser.uid;
            console.log("change logout");
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            document.getElementById("logout-btn").addEventListener("click", function () {
                firebase.auth().signOut().then(function () {
                    //create_alert("success", "");
                    alert("sueccess");
                }).catch(function (error) {
                    //create_alert("error", error.code, error.message);
                    alert("failed");
                });
            });
            //----
            var postsRef = firebase.database().ref('list/' + loginUser);
            var total_post = [];
            var first_count = 0;
            var second_count = 0;
            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        total_post[total_post.length] = '<p>' + childData.size + "</p>" + '<p>' + childData.num + '張</p>' + '<hr>';
                        first_count += 1;
                    });
                    document.getElementById('datalist').innerHTML = total_post.join('');

                    //add listener
                    postsRef.on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            total_post[total_post.length] = '<p>' + childData.size + "</p>" + '<p>' + childData.num + '張</p>' + '<hr>';
                            document.getElementById('datalist').innerHTML = total_post.join('');
                        }
                    });
                })
                .catch(e => console.log(e.message));
            /*var database = firebase.database().ref('user/' + loginUser);
            var dataname;
            var dataemail;
            var dataphone; 
            var dataaddr;

            var first_count = 0;
            var second_count = 0;
            var datakey;
            
            database.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                
            
                dataaddr = childSnapshot.val().addr;
                dataemail = childSnapshot.val().email;
                dataname = childSnapshot.val().name;
                console.log(dataname);
                dataphone = childSnapshot.val().phone;
                first_count +=1;
                
            });
            

            document.getElementById("datalist").innerHTML = '<p class="card-text">姓名:' + dataname + '</p>' + 
                '<p class="card-text">Email:' + dataemail + '</p>' + 
                '<p class="card-text">電話號碼:' + dataphone + '</p>' +
                '<p class="card-text">地址:' + dataaddr + '</p>';

            //console.log("once");
            //console.log(dataname);
            
            
        }).catch(e => console.log(e.message));*/

        } else {
            // It won't show any post if not login
            menu.innerHTML = '<a class="nav-link" href="signin.html">Login</a>';
            document.getElementById("dynamic_user").innerHTML = "";
        }
    });

    
    
    
    
}

window.onload = function () {
    init();
};
