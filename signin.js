function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var btnFB = document.getElementById('btnFB');
    var btnforget = document.getElementById("btnforget");

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(function () {
                window.location = "index.html";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnGoogle.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then(function () {
                var user = firebase.auth().currentUser;
                var newpostref = firebase.database().ref('user/' + user.uid).push();
                newpostref.set({
                    name: user.displayName,
                    email: user.email,
                    phone: "",
                    addr: ""
                });
                console.log(user.uid);
                alert("login");
                window.location = "index.html";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnFB.addEventListener('click', function (){
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            var loginUser = firebase.auth().currentUser;
                var newpostref = firebase.database().ref('user/' + loginUser.uid).push();
                newpostref.set({
                    name: loginUser.displayName,
                    email: loginUser.email,
                    phone: "",
                    addr: ""
                });
            window.location = "index.html";
            }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            create_alert("error", errorMessage);
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            });
    });
    btnSignUp.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        var name = document.getElementById("inputName").value;
        if (name != ""){
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(function () {
                var loginUser = firebase.auth().currentUser;
                var newpostref = firebase.database().ref('user/' + loginUser.uid).push();
                newpostref.set({
                    name: name,
                    email: email,
                    phone: "",
                    addr: ""
                });
                loginUser.updateProfile({
                    displayName: name,
                    photoURL: ""
                  }).then(function() {
                    // Update successful.
                    console.log("update profile success");
                  }).catch(function(error) {
                    // An error happened.
                    console.log("update profile error");
                  });
                create_alert("success", "You could sign in  right now!");
                txtEmail.value = "";
                txtPassword.value = "";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
        }
        else{
            create_alert("error", "Please fiil in the name!");
        }
        
    });

    btnforget.addEventListener("click", function(){
        var auth = firebase.auth();
        var emailAddress = txtEmail.value;

        auth.sendPasswordResetEmail(emailAddress).then(function() {
        // Email sent.
        create_alert("success", "The reset email has been sent to your email.");
        }).catch(function(error) {
        // An error happened.
        var errorCode = error.code;
        var errorMessage = error.message;
        create_alert("error", errorMessage);
        txtEmail.value = "";
        });
        });

}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
    else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
}