function init() {
    var user_email = '';
    var loginUser;
    var name;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic_signin');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = '<a class="nav-link" href="#" id="logout-btn">Logout</a>';
            document.getElementById("dynamic_user").innerHTML = '<a class="nav-link" href="dashboard.html" id="user-btn">'+user.displayName+'</a>';
            loginUser = firebase.auth().currentUser.uid;
            name = user.displayName
            console.log("change logout");
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            document.getElementById("logout-btn").addEventListener("click", function () {
                firebase.auth().signOut().then(function () {
                    //create_alert("success", "");
                    alert("sueccess");
                }).catch(function (error) {
                    //create_alert("error", error.code, error.message);
                    alert("failed");
                });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = '<a class="nav-link" href="signin.html">Login</a>';
            document.getElementById("dynamic_user").innerHTML = "";
            loginUser = "";
            name = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "" && loginUser) {
            var newpostref = firebase.database().ref('com_list').push();
            newpostref.set({
                email: name,
                data: post_txt.value
            });
            post_txt.value = "";
        }
        else{
            alert("請先登入");
        }
    });

    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";

    var str_after_content = "</p></div></div>\n";


    var postsRef = firebase.database().ref('com_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};
